package com.aprenderjava;

import javax.swing.border.EtchedBorder;

public class Estudiante extends Persona {

    byte idEstudiante;
    byte calificacion;

    public static void main(String[] args) {
        Persona objPersona = new Persona();
        Estudiante objEstudiante = new Estudiante();

        objEstudiante.idEstudiante = 20;
        objEstudiante.calificacion = 10;
        objPersona.nombre = "Carlos";
        objPersona.edad = 39;
        objPersona.direccion = "Calle 22 # 18 42";

        System.out.println("Datos del estudiante: ");
        System.out.println("ID: " + objEstudiante.idEstudiante);
        System.out.println("Nombre: " + objPersona.nombre);
        System.out.println("Edad: " + objPersona.edad);
        System.out.println("Dirección: " + objPersona.direccion);;
        System.out.println("Calificación: " + objEstudiante.calificacion);
    }


}
